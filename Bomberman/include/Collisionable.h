/*
 * Collisionable.h
 * El Personaje heredará esta interfaz y la implementará
 * El GamePlay la usará para determinar colisiones
 *  Created on: 4 ago. 2021
 *      Author: joe
 */

#ifndef INCLUDE_COLLISIONABLE_H_
#define INCLUDE_COLLISIONABLE_H_
#include <SFML/Graphics.hpp>

class Collisionable {
public:
	Collisionable();
	virtual bool isCollision(const Collisionable& _object) const =0;
	virtual sf::FloatRect getBounds() const =0;
	virtual ~Collisionable();
};

#endif /* INCLUDE_COLLISIONABLE_H_ */
