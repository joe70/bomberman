/*
 * GamePlay.h
 *
 *  Created on: 15 jul. 2021
 *      Author: joe
 */

#ifndef GAMEPLAY_H_
#define GAMEPLAY_H_

#include <SFML/Graphics.hpp>
#include "Personaje.h"
#include "Controller.h"
#include "Mapa.h"
#include "GamePlayable.h"
#include "Bomb.h"
#include <list>

class GamePlay : public sf::Drawable, public GamePlayable {
public:
	GamePlay();
	virtual ~GamePlay();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void command();
	void update();
	virtual void addBomb(sf::Vector2f position, int type) override;

private:
	Personaje _player;
	Controller _controller;
	Mapa _mapa;
	// ojo que las listas y los contenerdores de la STL crean una copia del objeto
	// y la meten en el contenedor, si lo sacas se destruye la copia.
	std::list<Bomb*> _bombs;

	void blockCollision();
	void bombWavesCollision(Bomb& b, const std::vector<Block*>& blocks, int i, int size, int incremento);
	void bombCollision(Bomb& b);

};

#endif /* GAMEPLAY_H_ */
