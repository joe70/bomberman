/*
 * Personaje.h
 *
 *  Created on: 15 jul. 2021
 *      Author: joe
 */

#ifndef INCLUDE_PERSONAJE_H_
#define INCLUDE_PERSONAJE_H_

#include <SFML/Graphics.hpp>
#include "Controller.h"
#include "Collisionable.h"
#include "GamePlayable.h"

class Personaje : public sf::Drawable, public sf::Transformable, public Collisionable {
public:
	Personaje(Controller&, GamePlayable &);
	virtual ~Personaje();

	virtual bool isCollision(const Collisionable& _object) const override;
	virtual sf::FloatRect getBounds() const override;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	void update();

	const sf::Vector2f& getVelocity() const;
protected:
	Controller& _controller;
	float _velocityForce;
	sf::RectangleShape _body;
	sf::Vector2f _velocity; // vale para saber si venimos desde arriba, abajo, izqd, derecha
	GamePlayable& _gamePlay;
	bool _isBomb;
	sf::Texture _texture;
	sf::Sprite _sprite;
	float _frame;
};
#endif /* INCLUDE_PERSONAJE_H_ */
