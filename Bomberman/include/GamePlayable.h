/*
 * GamePlayable.h
 * Interfaz para soltar la bomba que implementa GamePlay
 *  Created on: 4 sept. 2021
 *      Author: joe
 */

#ifndef INCLUDE_GAMEPLAYABLE_H_
#define INCLUDE_GAMEPLAYABLE_H_
#include <SFML/Graphics.hpp>

class GamePlayable {
public:
	GamePlayable();
	virtual void addBomb(sf::Vector2f position, int type) = 0;
	virtual ~GamePlayable();
};
#endif /* INCLUDE_GAMEPLAYABLE_H_ */
