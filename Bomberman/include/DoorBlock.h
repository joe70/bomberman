/*
 * DoorBlock.h
 *
 *  Created on: 4 sept. 2021
 *      Author: joe
 */

#ifndef DOORBLOCK_H_
#define DOORBLOCK_H_

#include <Block.h>

class DoorBlock: public Block {
public:
	DoorBlock();
	virtual ~DoorBlock();
};

#endif /* DOORBLOCK_H_ */
