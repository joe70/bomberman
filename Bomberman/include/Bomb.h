/*
 * Bomb.h
 *
 *  Created on: 4 sept. 2021
 *      Author: joe
 */

#ifndef BOMB_H_
#define BOMB_H_
#include<SFML/Graphics.hpp>
#include "Collisionable.h"


class Bomb : public sf::Drawable, public sf::Transformable, public Collisionable {

private:
	sf::Texture _texture;
	sf::Sprite _sprite;
	sf::CircleShape _body;
	sf::RectangleShape _waves[4]; // el area de explosión
	unsigned int _timeLive;
	unsigned int _state;
	int _size; // tamaño de la onda expansiva bomba
	bool _damage;
	float _frame;


public:
	Bomb(sf::Vector2f position);
	~Bomb();

	virtual bool isCollision(const Collisionable& _object) const override ;
	virtual sf::FloatRect getBounds() const override;
	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void update();

	bool isLive() const;
	bool isDamage() const;
	bool damage(Collisionable& body);

	int getSize() const;

};
#endif /* BOMB_H_ */
