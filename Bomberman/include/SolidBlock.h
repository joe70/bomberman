/*
 * SolidBlock.h
 *
 *  Created on: 3 ago. 2021
 *      Author: joe
 */

#ifndef INCLUDE_SOLIDBLOCK_H_
#define INCLUDE_SOLIDBLOCK_H_

#include "Block.h"

class SolidBlock: public Block {
public:
	SolidBlock();
	virtual ~SolidBlock();
};

#endif /* INCLUDE_SOLIDBLOCK_H_ */
