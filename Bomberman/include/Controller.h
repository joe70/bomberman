/*
 * Controler.h
 *
 *  Created on: 17 jul. 2021
 *      Author: joe
 */

#ifndef INCLUDE_CONTROLLER_H_
#define INCLUDE_CONTROLLER_H_

class Controller {
private:
	bool _buttons[6];

public:
	enum class Buttons {
			Up, Down, Left, Right, Button1, Button2
		};
	void reset();

	void push(Controller::Buttons button);
	bool isDown(Controller::Buttons button);
};



#endif /* INCLUDE_CONTROLLER_H_ */
