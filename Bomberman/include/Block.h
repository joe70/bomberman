/*
 * Block.h
 *
 *  Created on: 2 ago. 2021
 *      Author: joe
 */

#ifndef INCLUDE_BLOCK_H_
#define INCLUDE_BLOCK_H_

#include <SFML/Graphics.hpp>
#include "Collisionable.h"
// Los que heredan de transformable me están dando por saco, me toca clean y build project

class Block : public sf::Drawable, public sf::Transformable , public Collisionable{
protected:
	sf::RectangleShape _body;
	bool _solid;
	sf::Texture _texture;
	bool _breakable;

public:
	Block();
	virtual ~Block();

	virtual bool isCollision(const Collisionable& _object) const override ;
	virtual sf::FloatRect getBounds() const override;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void update();

	bool isSolid() const;

	bool isBreakable() const;

	virtual void toBreak();
};

#endif /* INCLUDE_BLOCK_H_ */
