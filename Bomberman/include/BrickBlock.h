/*
 * BrickBlock.h
 *
 *  Created on: 29 ago. 2021
 *      Author: joe
 */

#ifndef BRICKBLOCK_H_
#define BRICKBLOCK_H_

#include <SFML/Graphics.hpp>
#include <SolidBlock.h>

class BrickBlock: public SolidBlock {
public:
	BrickBlock();
	virtual ~BrickBlock();
	virtual void toBreak() override;

};

#endif /* BRICKBLOCK_H_ */
