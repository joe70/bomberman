/*
 * Mapa.cpp
 *
 *  Created on: 18 jul. 2021
 *      Author: joe
 */

#include "Mapa.h"
#include "SolidBlock.h"
#include "BrickBlock.h"
#include "DoorBlock.h"
#include <iostream>

Mapa::Mapa() {
	Block* _pBlock {nullptr};

	int i{0};
	for(int f=0; f<Mapa::Height; f++) {
		for (int c=0; c<Mapa::Width; c++) {
			switch (_vMap[i]) {
				case 1:
					_pBlock = new BrickBlock();
					break;
				case 17:
					_pBlock = new SolidBlock();
					break;
				case 33:
					_spawnPlayer.x = c;
					_spawnPlayer.y = f; // @suppress("No break at end of case")
					_pBlock = new DoorBlock;
					break;

				default:
					_pBlock = new Block();
					break;
			}
			// Donde va localizado cada block teniendo en cuenta que el tile es de 16x16
			_pBlock->setPosition(c*16.0, f*16.0);

			_blocks.push_back(_pBlock);
			i++;
		}
	}
}

Mapa::~Mapa() {

	for (Block* pBlock : _blocks)
			delete pBlock;
}

void Mapa::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	for (Block* pBlock : _blocks)
		target.draw(*pBlock, states);

}

void Mapa::update() {
}

sf::Vector2f Mapa::getPlayerSpawn() const {
	return _spawnPlayer;
}

const std::vector<Block*>& Mapa::getBlocks() const {
	return _blocks;
}
