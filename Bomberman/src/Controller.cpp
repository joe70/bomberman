/*
 * Controler.cpp
 *
 *  Created on: 17 jul. 2021
 *      Author: joe
 */

#include "Controller.h"
#include <iostream>
#include <cstring>

void Controller::reset() {
	std::memset(&_buttons, 0, 6);
}

void Controller::push(Controller::Buttons button) {
	_buttons[(int)button]=true;
}

bool Controller::isDown(Controller::Buttons button) {

	return _buttons[(int)button];
}

