/*
 * BrickBlock.cpp
 *
 *  Created on: 29 ago. 2021
 *      Author: joe
 */

#include "BrickBlock.h"

BrickBlock::BrickBlock() {
	// TODO Auto-generated constructor stub
	_breakable={true};
	// Sale con fondo rojo, ver constructor del solidBlock, ponemos white para que use toda la gama
	_body.setFillColor(sf::Color::White);

	// creamos un inrect para extraer el rectángulo que queremos, en este caso el primer tile del fichero de textura
	// posición 0,0 y ancho y alto 16

	// sf::IntRect rect {0,0,16,16};
	// _body.setTextureRect(rect);
	_body.setTextureRect({0,0,16,16}); // las dos instrucciones anteriores en una
}


BrickBlock::~BrickBlock() {
	// TODO Auto-generated destructor stub
}

void BrickBlock::toBreak() {
	Block::toBreak();

	_body.setTextureRect({0,4*16,16,16});
}
