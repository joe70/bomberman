/*
 * main.cpp
 *
 *  Created on: 15 jul. 2021
 *      Author: joe
 */
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "GamePlay.h"
#include <stdexcept>
#include <iostream>
#include "Bomb.h"


int main()
try {
 	srand(time(NULL));
	// Tenemos tiles de 16x16, 30 columnas y 20 filas, 16*30, 16*20
	// como sale pequeño lo doblamos a 960x640
	sf::RenderWindow window(sf::VideoMode(960, 640), "BomberMan");
	window.setFramerateLimit(60);

	sf::View view(sf::FloatRect(0.f, 0.f, 480.f, 320.f ));
	view.zoom(1);

	window.setView(view);

	//Bomb b({10,10});

	GamePlay gp;

	while(window.isOpen()) {
		window.clear();
		sf::Event event;
		while(window.pollEvent(event)) {
			if(event.type==sf::Event::Closed) {
				window.close();
			}
		}
		gp.command();

		gp.update();

		window.draw(gp);

		window.display();

	}
	return 0;
}
catch (std::exception& e) {
    std::cerr << "error: " << e.what() << '\n';
    return 1;    // 1 indicates failure
}


