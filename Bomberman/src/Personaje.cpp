/*
 * Personaje.cpp
 *
 *  Created on: 15 jul. 2021
 *      Author: joe
 */

#include <Personaje.h>
#include <iostream>

Personaje::Personaje(Controller &controller, GamePlayable& gamePlay) : \
					_controller(controller), _velocityForce(2), _velocity{}, _gamePlay(gamePlay) {
	// TODO Auto-generated constructor stub
	_body.setSize(sf::Vector2f(10,10));
	_isBomb=false;
	_body.setOrigin(5,5);

	if (!_texture.loadFromFile("Resources/man-2.png")) {

	}
	_sprite.setTexture(_texture);
	_sprite.setTextureRect({0,0,21,32}); // el primer frame
	_sprite.setScale(.75,.75);
	_sprite.setOrigin(21/2, 32/1.5); // para centrarlo

	_frame=0;
}


Personaje::~Personaje() {
	// TODO Auto-generated destructor stub
}


bool Personaje::isCollision(const Collisionable& _object) const {
	return getBounds().intersects(_object.getBounds());
}

sf::FloatRect Personaje::getBounds() const {
	sf::FloatRect rect = _body.getGlobalBounds();
	// TODO Investigar esto, en teoria left y top deberían ser coordenadas en el 2D World pero nada de eso
	// al final construye el rectángulo con las posiciones en x e y de más abajo que esas si son globales y con las
	// anchos y alto
	// std::cout << rect.top << ", " << rect.left << ", " << rect.width << ", " << rect.height << std::endl;

	rect.left += getPosition().x;
	rect.top += getPosition().y;

	// std::cout << rect.top << ", " << rect.left << ", " << rect.width << ", " << rect.height << std::endl;

	// sf::FloatRect rect_L = _body.getLocalBounds();
	// std::cout << rect_L.top << ", " << rect_L.left << ", " << rect_L.width << ", " << rect_L.height << std::endl;

	return rect;
}

void Personaje::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// combinaciones de transformaciones
	states.transform *=getTransform();

	//target.draw(_body, states);
	target.draw(_sprite, states);
}

void Personaje::update() {
// Si en lugar de if-else if cambiamos por if y también en Gameplay.cpp permite
// movimiento diagonal
	_velocity={}; // ha de inicializarse cada vez

	if (_controller.isDown(Controller::Buttons::Up)) {
		_velocity.y = -_velocityForce;
		// movernos entre los frame de animación
		_frame+=.2;
		if (_frame>5) {
			_frame=0;
		}
		_sprite.setTextureRect({21*(int)_frame,32*2,21,32}); // fila 2

	}
	else if (_controller.isDown(Controller::Buttons::Right)) {
		_velocity.x = _velocityForce;
		// movernos entre los frame de animación
				_frame+=.2;
				if (_frame>5) {
					_frame=0;
				}
				_sprite.setTextureRect({21*(int)_frame,32*1,21,32}); // fila 1

	}
	else if (_controller.isDown(Controller::Buttons::Down)) {
		_velocity.y = _velocityForce;

		// movernos entre los frame de animación
		_frame+=.2;
		if (_frame>5) {
			_frame=0;
		}
		_sprite.setTextureRect({21*(int)_frame,0,21,32}); // fila 0
	}
	else if (_controller.isDown(Controller::Buttons::Left)) {
		_velocity.x = - _velocityForce;
		// movernos entre los frame de animación
		_frame+=.2;
		if (_frame>5) {
			_frame=0;
		}
		_sprite.setTextureRect({21*(int)_frame,32*3,21,32}); // fila 3
	}
	// poner Bomba
	if (_controller.isDown(Controller::Buttons::Button1)) {
		if (!_isBomb) {
			_isBomb=true;
			_gamePlay.addBomb(getPosition(),1);
		}
	}
	else {
		_isBomb=false;
	}

	move(_velocity);
}

const sf::Vector2f& Personaje::getVelocity() const {
	return _velocity;
}
