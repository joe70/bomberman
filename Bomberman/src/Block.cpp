/*
 * Block.cpp
 *
 *  Created on: 2 ago. 2021
 *      Author: joe
 */

#include <Block.h>

Block::Block() : _solid(false), _breakable(false) {

	_body.setSize(sf::Vector2f(16.f,16.f));
	//_body.setOutlineColor(sf::Color::Black);
	// _body.setOutlineThickness(3);
	if (!_texture.loadFromFile("Resources/set.png")) {

	}
	_body.setTexture(&_texture);

	// creamos un inrect para extraer el rectángulo que queremos, en este caso el primer tile del ficheor de textura
	// posición 0,0 y ancho y alto 16
	//sf::IntRect rect {0,0,16,16};
	//_body.setTextureRect(rect);
	_body.setTextureRect({3*16,2*16,16,16});
}

Block::~Block() {
	// TODO Auto-generated destructor stub
}

bool Block::isCollision(const Collisionable& _object) const {
	return getBounds().intersects(_object.getBounds());
}

sf::FloatRect Block::getBounds() const {
	sf::FloatRect rect = _body.getGlobalBounds();

	rect.left = getPosition().x;
	rect.top = getPosition().y;

	return rect;
}


void Block::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	states.transform *= getTransform();
	target.draw(_body, states);
}

void Block::update() {
}

bool Block::isSolid() const {
	return _solid;
}

bool Block::isBreakable() const {
	return _breakable;
}

void Block::toBreak() {
	_breakable = false;
	_solid = false;
}
