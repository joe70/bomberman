/*
 * GamePlay.cpp
 *
 *  Created on: 15 jul. 2021
 *      Author: joe
 */

#include "GamePlay.h"
#include <iostream>

GamePlay::GamePlay() : _player(_controller, *this) {
	_player.setPosition(_mapa.getPlayerSpawn().x*16 + (16/2-5), _mapa.getPlayerSpawn().y*16+(16/2-5));
	// std::cout << "pos_: x" << _player.getPosition().x << "pos_y:" << _player.getPosition().y << std::endl;
}

GamePlay::~GamePlay() {
	// TODO Auto-generated destructor stub
}

void GamePlay::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(_mapa, states);

	for (const Bomb* b : _bombs) {
		target.draw(*b, states);
	}

	target.draw(_player, states);
}

void GamePlay::command() {
	_controller.reset();
// Si en lugar de if-else if cambiamos por if y también en Personaje.cpp permite
// movimiento diagonal
// Si pulso dos botones a la vez se ejecuta el que está más arriba en estos if anidados
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
		_controller.push(Controller::Buttons::Up);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		_controller.push(Controller::Buttons::Right);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
		_controller.push(Controller::Buttons::Down);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		_controller.push(Controller::Buttons::Left);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
			_controller.push(Controller::Buttons::Button1);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
			_controller.push(Controller::Buttons::Button2);
	}

}

void GamePlay::blockCollision() {
	const std::vector<Block*>& blocks = _mapa.getBlocks();

	for (Block* pBlock : blocks) {
		if (pBlock->isSolid() &&_player.isCollision(*pBlock)) {
			// para corregir actuamos según desde donde venga
			if (_player.getVelocity().y > 0) { // el player baja y choca contra un bloque solid
				//float diferencia = (_player.getBounds().top+_player.getBounds().height)-(pBlock->getBounds().top);
				//_player.move(0.0f,-diferencia); // fallaron los paréntesis
				_player.move(0.0f,-((_player.getBounds().top+_player.getBounds().height)-pBlock->getBounds().top));
			}
			else if (_player.getVelocity().y < 0) { // el player sube y da contra un bloque solid
				_player.move(0.0f,(pBlock->getBounds().top + pBlock->getBounds().height)-_player.getBounds().top);
			}
			else if (_player.getVelocity().x > 0) {
				_player.move(-((_player.getBounds().left + _player.getBounds().width)-(pBlock->getBounds().left)), 0.0f);
			}
			else if	(_player.getVelocity().x < 0) {
				_player.move((pBlock->getBounds().left + pBlock->getBounds().width-_player.getBounds().left), 0.0f);
			}
		}
	}

}

void GamePlay::update() {
	_mapa.update();
	_player.update();

	// bucle que recorre las bombas y se comporta según su estado, borra si
	// ha pasado el tiempo de vida o comprueba si está explotando y por tanto si daña
	std::list<Bomb*>::iterator i = _bombs.begin();
	while (i != _bombs.end()) {
		Bomb& bomb = (**i);
		bomb.update();

		if (!bomb.isLive()) {
			// si no hagoldelete no libera memoria.
			delete *i;
			i = _bombs.erase(i);
		}
		else {
			i++;
			if (bomb.isDamage()) {
				bombCollision(bomb);
			}
		}
	}

	blockCollision();

}

void GamePlay::addBomb(sf::Vector2f position, int type) {
	bool exist = false; // para hacer que si ya hay una bomba no ponga otra

	// recordemos que cada tile es 16x16, con esto siempre en el centro
	// de cada tile
	position.x = ((int)position.x/16) * 16.f;
	position.y = ((int)position.y/16) * 16.f;

	// para que si ya hay una bomba no ponga otra
	for (const Bomb* b : _bombs) {
		if (b->getBounds().contains(position)) {
			exist = true;
			break;
		}
	}

	if (exist==false) {
		_bombs.push_back(new Bomb(position));
	}
}

void GamePlay::bombWavesCollision(Bomb& b, const std::vector<Block*>& blocks,
		int i, int size, int incremento) {
	i +=incremento;
	for (int k=0; k<size; k++) {
		if (blocks[i]->isBreakable() && b.damage(*blocks[i])) {
			blocks[i]->toBreak();
			break;
		}
		else if (blocks[i]->isSolid() && b.damage(*blocks[i])) {
			// con esto evitamos que rompa por detrás de los sólidos
			break;
		}
		else {
			i+=incremento;
		}
	}
}

void GamePlay::bombCollision(Bomb& b) {

	const std::vector<Block*>& blocks = _mapa.getBlocks();
	int x,y;
	x=b.getPosition().x / 16;
	y=b.getPosition().y / 16;

	int posActual = y*30+x; // 30 es el ancho del mapa pero es un vector
	// que en vez de recorrer todos los bloques analice los que puede romper en
	// las inmediaciones de la bomba

	bombWavesCollision(b, blocks, posActual, std::min<int>(30-x, b.getSize()), 1);
	bombWavesCollision(b, blocks, posActual, std::min<int>(x, b.getSize()), -1);
	//siguiente fila
	bombWavesCollision(b, blocks, posActual, std::min<int>(20-y, b.getSize()), 30);
	bombWavesCollision(b, blocks, posActual, std::min<int>(y, b.getSize()), -30);

}


