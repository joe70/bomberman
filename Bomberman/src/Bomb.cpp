/*
 * Bomb.cpp
 *
 *  Created on: 4 sept. 2021
 *      Author: joe
 */

#include "Bomb.h"
#include <iostream>

Bomb::Bomb(sf::Vector2f position) : _size(2), _damage(true), _frame(0) {
	// TODO Auto-generated constructor stub
	if (!_texture.loadFromFile("Resources/man-2.png")) {

	}
	_sprite.setTexture(_texture);
	_sprite.setTextureRect({175,9,18,18}); // el primer frame
	// _sprite.setScale(.75,.75);
	//_sprite.setOrigin(18/2, 18/2);

	_body.setRadius(8);
	// _body.setFillColor(sf::Color::Black);

	setPosition(position);

	// inicialización del area de explosión. Recordar que los tiles son de 16x16
	// superior
	_waves[0].setSize(sf::Vector2f(16,16*_size));
	_waves[0].setPosition(0,-(16*_size));
	// derecha
	_waves[1].setSize(sf::Vector2f(16*_size,16));
	_waves[1].setPosition(16,0);
	// inferior
	_waves[2].setSize(sf::Vector2f(16,16*_size));
	_waves[2].setPosition(0,16);
	// izquierda
	_waves[3].setSize(sf::Vector2f(16* _size,16));
	_waves[3].setPosition(-(16*_size),0);

	_timeLive= 100; // 60 es un segundo

	_state =0;

}

Bomb::~Bomb() {
	// TODO Auto-generated destructor stub
}

bool Bomb::isCollision(const Collisionable& _object) const {
	return getBounds().intersects(_object.getBounds());
}

sf::FloatRect Bomb::getBounds() const {
	sf::FloatRect rect = _body.getGlobalBounds();
	// TODO Investigar esto, en teoria left y top deberían ser coordenadas en el 2D World pero nada de eso
	// al final construye el rectángulo con las posiciones en x e y de más abajo que esas si son globales y con las
	// anchos y alto
	// std::cout << rect.top << ", " << rect.left << ", " << rect.width << ", " << rect.height << std::endl;

	rect.left += getPosition().x;
	rect.top += getPosition().y;

	// std::cout << rect.top << ", " << rect.left << ", " << rect.width << ", " << rect.height << std::endl;

	return rect;
}

void Bomb::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// combinaciones de transformaciones
	states.transform *=getTransform();
	target.draw(_sprite, states);

	if (_state == 1) {
		for (const sf::RectangleShape& r : _waves) {
			target.draw(r, states);
		}
	}
}

void Bomb::update() {

	switch (_state) {
		case 1: // se ha terminado el tiempo y está explotando
			_body.setFillColor(sf::Color::Red);
			--_timeLive;
			if (_timeLive == 0) {
				_state=2;
			}
			break;
		case 2: // está muerta

			break;
		case 0: // estado inicial
			--_timeLive;
			if (_timeLive == 0) {
					_state = 1;
					_timeLive = 20;
			}
			_frame+=0.15;
			if (_frame>3) {
				_frame=0;
			}
			_sprite.setTextureRect({175+((int)_frame*18),9,18,18}); // fila 1
			break;
		default:
			break;
	}

}

bool Bomb::isLive() const {
	return _state != 2;
}

bool Bomb::isDamage() const {
	return _state==1 && _damage;
}

bool Bomb::damage(Collisionable& body) {
	bool result = false;
	for (const sf::RectangleShape& wave : _waves) {
		sf::FloatRect rect = wave.getGlobalBounds();
		rect.left += getPosition().x;
		rect.top += getPosition().y;

		result = result || rect.intersects(body.getBounds());
	}

	// result = result || _body.getGlobalBounds().intersects(body.getBounds());
	if (result) {
		_damage=false;
	}
	return result;
}

int Bomb::getSize() const {
	return _size;
}
